package com.fc.test.common.exception;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fc.test.common.domain.AjaxResult;
import com.google.gson.Gson;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.UnauthenticatedException;
import org.apache.shiro.authz.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import java.io.IOException;
import java.io.PrintWriter;

/**
 * 全局异常处理
 * ajax请求返回错误信息
 * 页面请求跳转错误页面
 */
public class GlobalExceptionResolver  implements HandlerExceptionResolver{
	private static Logger logger = LoggerFactory.getLogger(GlobalExceptionResolver.class);

	@Override
    public ModelAndView resolveException(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex) {
		ModelAndView mv;
		//开发环境打印异常，正式环境请注销
    	ex.printStackTrace();
		
        //shiro异常拦截 
        if((ex instanceof UnauthorizedException)||(ex instanceof AuthorizationException)){
            //未授权(没有权限)异常
            if (isAjaxRequest(request)) {
                mv=new ModelAndView(new MappingJackson2JsonView());
                mv.addObject("code",403);
                mv.addObject("msg","没有操作权限");
            }else {
                mv = new ModelAndView("/error/403");
            }
            return mv;
        }else if((ex instanceof UnauthenticatedException)||(ex instanceof AuthenticationException)){
        	//未认证(登录)异常
            if (isAjaxRequest(request)) {
                mv=new ModelAndView(new MappingJackson2JsonView());
                mv.addObject("code",403);
                mv.addObject("msg","没有操作权限");
            }else {
                mv = new ModelAndView("/login");
            }
            return mv;
        }
        else {
            mv = new ModelAndView();
            logger.error(ExceptionUtils.getFullStackTrace(ex));
            return mv;

        }
	}
    /*
    * 判断请求是否是ajax请求
    * */
    public boolean isAjaxRequest(HttpServletRequest request) {
        String requestedWith = request.getHeader("x-requested-with");
        if (requestedWith != null && requestedWith.equalsIgnoreCase("XMLHttpRequest")) {
            return true;
        } else {
            return false;
        }
    }
}
